// Get dependencies

const express = require('express');
const bodyParser = require('body-parser');
const mongodb = require("mongodb");
const ObjectID = mongodb.ObjectID;

const USERS_COLLECTION = "users";
const ITEM_COLLECTION = "items";

// Get our API routes
const api = require('./server/routes/api');

const app = express();

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Create link to Angular build directory
let distDir = __dirname + "/dist/";
app.use(express.static(distDir));


// Create a database variable outside of the database connection callback to reuse the connection pool in your app.
let db;

// Connect to the database before starting the application server.
mongodb.MongoClient.connect(process.env.MONGODB_URI, function (err, database) {

  if (err) {
    console.log(err);
    process.exit(1);
  }

  // Save database object from the callback for reuse.
  db = database;
  console.log("Database connection ready");

  // Initialize the app.
  let server = app.listen(process.env.PORT || 8080, function () {
    let port = server.address().port;
    console.log("App now running on port", port);
  });
});

// CONTACTS API ROUTES BELOW

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({"error": message});
}

app.post("/api/checkUser", function(req, res) {
  let checkUser = req.body;
  console.log(checkUser);
  if (!checkUser.username || !checkUser.password){
    handleError(res,"Invalid user input","Must provide name and password",400);
  }else {
    console.log(checkUser.username);
    console.log(checkUser.password);
  db.collection(USERS_COLLECTION).find({username:  checkUser.username, password: checkUser.password}).toArray(function (err, docs) {
    if(err){
      handleError(res,err.message,"Failed to load users");
    } else {
      console.log(docs[0]);
      res.status(200).json(docs[0]);
    }
  });
  }
});

app.post("/api/addUser", function(req, res) {
  let newUser = req.body;

  if (!req.body.username) {
    handleError(res, "Invalid user input", "Must provide a name.", 400);
  }

  db.collection(USERS_COLLECTION).insertOne(newUser, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to create new user.");
    } else {
      res.status(201).json(doc.ops[0]);
    }
  });
});

app.post("/api/addItem", function (req, res) {
  let newItem = req.body;

  db.collection(ITEM_COLLECTION).insertOne(newItem, function (err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to add item");
    } else {
      res.status(201).json(doc.ops[0]);
    }
  })
});

app.get("/api/getItems", function (req, res) {
  db.collection(ITEM_COLLECTION).find().toArray(function (err, doc) {
    if (err) {
      handleError(res, err.message)
    } else {
      res.status(201).json(doc)
    }
  });
});
