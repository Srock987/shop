import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class StateService {
  currentState: State;
  stateProvider: BehaviorSubject<State>;
  constructor() {
    this.currentState = State.Browse;
    this.stateProvider = new BehaviorSubject(this.currentState);
  }
  get getState(){
    return this.stateProvider.asObservable();
  }
  changeState(newState: State) {
    this.currentState = newState;
    this.stateProvider.next(this.currentState);
  }


}

export enum State {
  Browse = 1,
  Basket,
  Order,
  Login,
  Register,
  Workspace,
  AddItem
}
