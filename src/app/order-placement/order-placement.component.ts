import { Component, OnInit } from '@angular/core';
import {State, StateService} from '../stateService/state.service';
import {FormBuilder, FormControl, FormGroup, NgControl, Validators} from '@angular/forms';
import {OrderService} from '../order.service/order.service';

@Component({
  selector: 'app-order-placement',
  templateUrl: './order-placement.component.html',
  styleUrls: ['./order-placement.component.css']
})
export class OrderPlacementComponent implements OnInit {
  userInfoFormGroup: FormGroup;
  formSubmitAttempt: boolean;
  orderSubmitted: boolean;
  constructor(private stateService: StateService, private formBuilder: FormBuilder, private orderService: OrderService) {
    this.createForm();
  }

  ngOnInit() {
  }
  backToCheckout() {
    this.stateService.changeState(State.Basket);
  }
  createForm() {
    this.userInfoFormGroup = this.formBuilder.group({
      name: [null, [Validators.required, Validators.minLength(3)]],
      address: [null, [Validators.required, Validators.minLength(3)]]
    });
  }
  isFieldValid(field: string) {
    return (!this.userInfoFormGroup.get(field).valid && this.userInfoFormGroup.get(field).touched);
  }
  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }
  onSubmit() {
    this.formSubmitAttempt = true;
    this.validateAllFormFields(this.userInfoFormGroup);
    if (this.userInfoFormGroup.status === 'VALID') {
      console.log('VALIDATED');
      this.orderSubmitted = true;
      this.orderService.placeOrder(this.userInfoFormGroup.value);
    }

  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      control.markAsTouched({ onlySelf: true });
    });
  }
}
