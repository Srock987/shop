export class User {

  constructor(username: string, password: string, privilege: Privilege = Privilege.NORMAL) {
    this.username = username;
    this.password = password;
    this.privilege = privilege;
  }


  username: string;
  password: string;
  privilege: Privilege;
  toJson(): string {
    return '{"username":' + this.username + ',' +
      '"password":' + this.password + ',' +
      '"privilege":' + this.privilege.toString() + '}';
  }
}

enum Privilege {
  NORMAL,
  ADMIN
}
