import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../entities/user';
import {UserDataService} from '../user-data.service/user-data.service';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {
  form: FormGroup;
  user: User;
  constructor(private fb: FormBuilder, private userDataService: UserDataService) {
    this.buildForm();
  }

  ngOnInit() {
  }

  buildForm() {
    this.form = this.fb.group({
      'username': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15)])],
      'password': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15)])]
    });
  }
  registerUser(user: User) {
    this.userDataService.addUser(user).subscribe(registeredUser => {
      this.user = registeredUser;
    });
  }

}
