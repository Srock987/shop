import {Injectable} from '@angular/core';
import {Item} from '../entities/item';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/filter';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/operator/mergeMap';
import {from} from 'rxjs/observable/from';
import {ItemDataService} from '../item-data.service/item-data.service';


const noFilter = 'noFilter';
const itemsConst: Item[] = [{
  id: 0,
  name: 'Garnek',
  description: 'Bardzo ładny garnek',
  price: 10,
  category: 'AGD'
},
  {
    id: 1,
    name: 'Cos',
    description: 'Bardzo ładny garnek',
    price: 20,
    category: 'RTV'
  },
  {
    id: 2,
    name: 'Garnek1',
    description: 'Bardzo ładny garnek',
    price: 20,
    category: 'AGH'
  }];

@Injectable()
export class ItemProviderService {
  private filterCategory: string;
  private itemsObs: BehaviorSubject<Item[]>;
  private dataStore: {
    items: Item[];
  };

  constructor(private itemDataService: ItemDataService) {
    this.dataStore = {items: []};
    this.itemsObs = <BehaviorSubject<Item[]>>new BehaviorSubject([]);
    this.setFilter(noFilter);
  }

  setFilter(newFilter: string) {
    this.filterCategory = newFilter;
    console.log('Filter set to' + this.filterCategory);
    this.loadItems();
  }

  getItemCategories() {
    const categories: string[] = [];
    for (const item of itemsConst) {
      if (categories.indexOf(item.category) === -1) {
        categories[categories.length] = item.category;
      }
    }
    return categories;
  }

  loadItems() {
    this.itemDataService.getItems().subscribe(itemsData => {
      this.dataStore.items = itemsData;
      this.itemsObs.next(Object.assign({}, this.dataStore).items);
    });
  }

  get itemsObservable() {
    return this.itemsObs.asObservable()
      .mergeMap(items => from(items)
        .filter(item => this.filterCategory === noFilter || item.category === this.filterCategory)
        .toArray());
  }
}
