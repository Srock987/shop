import { Injectable } from '@angular/core';
import {Item} from '../entities/item';
import 'rxjs/add/operator/map';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {from} from 'rxjs/observable/from';
import 'rxjs/add/operator/reduce';
import {OrderItem} from '../entities/orderItem';


@Injectable()
export class BasketService {
  private dataStore: {
    orderItems: OrderItem[]
  };
  private items: BehaviorSubject<OrderItem[]>;
  constructor() {
    this.dataStore = {
      orderItems: []};
    this.items = new BehaviorSubject(this.dataStore.orderItems);
  }
  get numberOfItems(){
    return this.items.flatMap(values => from(values)
      .map(item => item.quantity)
      .reduce((acc, val) => acc + val, 0));
  }
  get summaryPrice(){
    return this.items.flatMap(values => from(values)
      .reduce((acc, val) => acc + val.item.price * val.quantity, 0));
  }
  get itemsArray(){
    console.log(this.items);
    return this.items.asObservable();
  }
  addItem(item: Item) {
    const index = this.dataStore.orderItems.findIndex((value) => value.item.id === item.id);
    if (index > -1) {
      this.dataStore.orderItems[index].quantity++;
    }else {
      this.dataStore.orderItems[this.dataStore.orderItems.length] = {item: item, quantity: 1};
    }
    this.items.next(this.dataStore.orderItems);
  }
  removeItem(id: number) {
    const index = this.dataStore.orderItems.findIndex((value) => value.item.id === id);
    if (this.dataStore.orderItems[index].quantity > 1) {
      this.dataStore.orderItems[index].quantity--;
    }else {
      this.dataStore.orderItems.splice(index, 1);
    }
    this.items.next(this.dataStore.orderItems);
  }
  resetBasket() {
    this.dataStore.orderItems = [];
    this.items.next(this.dataStore.orderItems);
  }
}
