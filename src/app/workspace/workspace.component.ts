import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {UserDataService} from '../user-data.service/user-data.service';
import {User} from '../entities/user';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class WorkspaceComponent implements OnInit {
  users: User;
  constructor(private dataService: UserDataService) { }

  ngOnInit() {

  }
  getUser(user: User) {
    this.dataService.checkUser(user).subscribe( data => {
      this.users = data;
      console.log(data);
    });
  }
  addUser() {
    const addUser = new User('username1', 'password1');
    this.dataService.addUser(addUser).subscribe( data => {
      console.log(data);
    });
  }

}
