import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {OrderItem} from '../entities/orderItem';

@Component({
  selector: 'app-basket-item',
  templateUrl: './basket-item.component.html',
  styleUrls: ['./basket-item.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BasketItemComponent implements OnInit {
  @Input() orderItem: OrderItem;
  constructor() { }

  ngOnInit() {
  }

}
