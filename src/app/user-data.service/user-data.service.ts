import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {User} from '../entities/user';

const checkUserUrl = '/api/checkUser';
const addUserUrl = '/api/addUser';

@Injectable()
export class UserDataService {
  constructor(private http: HttpClient) { }

  checkUser(user: User) {
    return this.http.post(checkUserUrl, user).map(data => data as User);
  }

  addUser(user: User) {
    return this.http.post(addUserUrl, user).map(data => data as User);
  }

}
