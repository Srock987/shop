import { Injectable } from '@angular/core';
import {BasketService} from '../basket.service/basket.service';
import {OrderInfo} from '../entities/orderInfo';

@Injectable()
export class OrderService {

  constructor(private basketService: BasketService) { }
  placeOrder(userInfo: OrderInfo) {
    this.saveOrder(userInfo);
    this.basketService.resetBasket();
  }
  saveOrder(userInfo: OrderInfo) {
    console.log(userInfo);
     // TODO SAVING ORDERS
  }

}
