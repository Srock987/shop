import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {ItemDetailComponent} from './item-list-n-detail/app.item-detail.component';
import {ItemListComponent} from './item-list-n-detail/app.item-list.component';
import {CategoryItemComponent} from './category-list-n-item/app.category-item.component';
import {CategoryListComponent} from './category-list-n-item/app.category-list.component';
import {ItemProviderService} from './item-provider.service/item-provider.service';
import {BasketService} from './basket.service/basket.service';
import { TopBasketComponent } from './top-basket/top-basket.component';
import { BasketComponent } from './basket/basket.component';
import { BasketItemComponent } from './basket-item/basket-item.component';
import {StateService} from './stateService/state.service';
import { OrderPlacementComponent } from './order-placement/order-placement.component';
import {ReactiveFormsModule} from '@angular/forms';
import { FieldErrorDisplayComponent } from './field-error-display/field-error-display.component';
import {OrderService} from './order.service/order.service';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import {HttpClientModule} from '@angular/common/http';
import { WorkspaceComponent } from './workspace/workspace.component';
import {UserDataService} from './user-data.service/user-data.service';
import {HttpModule} from '@angular/http';
import { AddItemComponent } from './add-item/add-item.component';
import {ItemDataService} from './item-data.service/item-data.service';



@NgModule({
  declarations: [
    AppComponent,
    ItemDetailComponent,
    ItemListComponent,
    CategoryListComponent,
    CategoryItemComponent,
    TopBasketComponent,
    BasketComponent,
    BasketItemComponent,
    OrderPlacementComponent,
    FieldErrorDisplayComponent,
    UserLoginComponent,
    UserRegisterComponent,
    WorkspaceComponent,
    AddItemComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [ItemProviderService,
    BasketService,
    StateService,
    OrderService,
    UserDataService,
    ItemDataService
    ],
  bootstrap: [AppComponent,
    ItemDetailComponent,
    ItemListComponent,
    CategoryItemComponent,
    CategoryListComponent,
    TopBasketComponent]
})
export class AppModule {
}
