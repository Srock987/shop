import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../entities/user';
import {State, StateService} from '../stateService/state.service';
import {UserDataService} from '../user-data.service/user-data.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  form: FormGroup;
  user: User;
  constructor(private fb: FormBuilder, private stateService: StateService, private userDataService: UserDataService) {
    this.buildForm();
  }

  ngOnInit() {
  }

  buildForm() {
    this.form = this.fb.group({
      'username': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15)])],
      'password': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15)])]
    });
  }

  login(user: User) {
    this.userDataService.checkUser(user).subscribe( checkedUser => {
      this.user = checkedUser;
    });
  }

  goToRegistration() {
    this.stateService.changeState(State.Register);
  }
}
