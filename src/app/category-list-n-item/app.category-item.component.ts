import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-category-item',
  template: `
  <div class="categoryRow" *ngIf="category">
    <label>{{category}}</label>
  </div>
  `,
  styles: [`
  .categoryRow{
    margin: 10px;
    background: #e2bc2e;
  }
  `]
})

export class CategoryItemComponent {
  @Input() category: string;
}
