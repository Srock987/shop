import {Component} from '@angular/core';
import {ItemProviderService} from '../item-provider.service/item-provider.service';

@Component({
  selector: 'app-category-list',
  template: `
    <div class="categoryList">
      <div>
        <app-category-item [category]="'Home page'" (click)="setFilter('noFilter')"></app-category-item>
      </div>
      <div *ngFor="let category of categories">
        <app-category-item *ngIf="category" [category]="category" (click)="setFilter(category)"></app-category-item>
      </div>
    </div>
  `,
  styles: [`
  .categoryList {
    padding: 20px;
    background: #13e259;
  }
  `]
})

export class CategoryListComponent {
  categories: string[];
  constructor(private itemProviderService: ItemProviderService) {
  this.categories = itemProviderService.getItemCategories();
  }
  setFilter (filter: string) {
    this.itemProviderService.setFilter(filter);
  }
}
