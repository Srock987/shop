import {Component, OnInit} from '@angular/core';
import {State, StateService} from './stateService/state.service';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  template: `
    <div class="container-full" *ngIf="currentState | async; let state">
      <div class="topBasketBar">
        <div class="row">
          <div class="col-1">
            <button class="topButton" (click)="changeState(stateType.Browse)">Home</button>
          </div>
          <div class="col-1">
            <button class="topButton" (click)="changeState(stateType.Login)">Login</button>
          </div>
          <div class="col-1">
            <button class="topButton" (click)="changeState(stateType.AddItem)">Add Item</button>
          </div>
          <div class="col-1">
            <button class="topButton" (click)="changeState(stateType.Workspace)">Workspace</button>
          </div>
          <div class="col">
            <app-top-basket></app-top-basket>
          </div>
        </div>
      </div>
      <div class="row" *ngIf="state === stateType.Browse">
        <div class="col-3">
          <app-category-list></app-category-list>
        </div>
        <div class="col-9">
          <app-item-list></app-item-list>
        </div>
      </div>
      <div class="row" *ngIf="state === stateType.Basket">
        <div class="col" align="center">
          <app-basket></app-basket>
        </div>
      </div>
      <div class="row" *ngIf="state === stateType.Order">
        <div class="col" align="center">
          <app-order-placement></app-order-placement>
        </div>
      </div>
      <div class="row" *ngIf="state === stateType.Login">
        <div class="col" align="center">
          <app-user-login></app-user-login>
        </div>
      </div>
      <div class="row" *ngIf="state === stateType.Register">
        <div class="col" align="center">
          <app-user-register></app-user-register>
        </div>
      </div>
      <div class="row" *ngIf="state === stateType.Workspace">
        <div class="col" align="center">
          <app-workspace></app-workspace>
        </div>
      </div>
      <div class="row" *ngIf="state === stateType.AddItem">
        <div class="col" align="center">
          <app-add-item></app-add-item>
        </div>
      </div>
    </div>    `,
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  stateType: any = State;
  currentState: Observable<State>;
  constructor(private stateService: StateService) {
  }

  ngOnInit(): void {
    this.currentState = this.stateService.getState;
  }
  changeState(state: State) {
    this.stateService.changeState(state);
  }

}
