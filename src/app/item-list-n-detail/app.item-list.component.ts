import {Component, OnInit} from '@angular/core';
import {Item} from '../entities/item';
import {ItemProviderService} from '../item-provider.service/item-provider.service';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-item-list',
  template: `
    <div class="itemList">
      <div *ngFor="let item of itemsObservables | async">
       <app-item-detail [item]="item"></app-item-detail>
      </div>
    </div>
  `
})


export class ItemListComponent implements OnInit {
  itemsObservables: Observable<Item[]>;
  constructor(private itemProviderService: ItemProviderService) {
  }

  ngOnInit(): void {
    this.itemsObservables = this.itemProviderService.itemsObservable;
    this.itemProviderService.loadItems();
  }
}
