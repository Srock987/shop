import {Component, Input} from '@angular/core';
import {Item} from '../entities/item';
import {BasketService} from '../basket.service/basket.service';

@Component({
  selector: 'app-item-detail',
  template: `
    <div class="itemDetail" *ngIf="item">
      <div class="row">
        <div class="col">
          <h6>{{item.name}}</h6>
          <label>{{item.description}}</label>
        </div>
        <div class="col">
          <button (click)="addToBasket()">Add to basket</button>
        </div>
        <div class="col" style="align-items: center">
          <div class="priceTag">
            <label>{{item.price}}</label>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .itemDetail {
      padding: 20px;
      background: green;
    }

    .priceTag {
      padding: 10px;
      background: #0054e2;
    }
  `]
})

export class ItemDetailComponent {
  @Input() item: Item;
  constructor(private basketService: BasketService) {
  }
  addToBasket() {
    this.basketService.addItem(this.item);
  }
}
